/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.bank;

import java.util.Scanner;

/**
 *
 * @author extre
 */
public class Bank {
    
    private static Double getValor() {
        System.out.println("Digite o valor: ");
        Scanner valor = new Scanner(System.in);
        return valor.nextDouble();
    }

    @SuppressWarnings("empty-statement")
    public static void main(String[] args) {
        
        double saldo = 0;
        String opcao = "";
        
        while(!opcao.equals("0")){
            System.out.println("Bem-vindo Jones Radtke ao HomeBank SENAC!");
            System.out.println("#########################################");
            System.out.println("# 1. Ver extrato da conta               #");
            System.out.println("# 2. Depósito em conta                  #");
            System.out.println("# 3. Saque da conta                     #");
            System.out.println("# 0. Sair                               #");
            System.out.println("#########################################");
            System.out.println("Digite uma das opções acima:");
            Scanner tc = new Scanner(System.in);
            opcao = tc.next();

            switch (opcao) {
                case "1":
                    System.out.println("Saldo:.....R$: " + saldo);
                    break;
                case "2":
                    saldo = saldo + getValor();
                    System.out.println("Saldo:.....R$: " + saldo);
                    break;
                case "3":
                    saldo = saldo - getValor();
                    System.out.println("Saldo:.....R$: " + saldo);
                    break;
                case "0":
                    System.out.println("Saindo da aplicação...");
                    break;
                default:
                    System.out.println("Opção inválida!");
            }
        }
        
    }

    
}
